package com.atguigu.juc.Test1216.E_Lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyThread implements Runnable {
    int count=100;
    Lock lock= new ReentrantLock();
    @Override
    public void run() {
        while (true) {
            lock.lock();
            if (count > 0) {
                System.out.println(Thread.currentThread().getName() + "卖出了第" + (count --) + "张票");
            }
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }
    }
}
