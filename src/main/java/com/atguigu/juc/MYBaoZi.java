package com.atguigu.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: MYBaoZi
 * @Desc:
 * @create: 2022-10-17 17:01
 **/
class BaoZiPu{
    private boolean flag=false;
    private final Lock lock=new ReentrantLock();
    private final Condition proud= lock.newCondition();
    private final Condition xiaofeizhe= lock.newCondition();
    private int count=0;

    public void shengcan()
    {
        lock.lock();
        try {
            while (flag!=false)
        { proud.await(); }
        System.out.println(Thread.currentThread().getName()+"店铺生产第"+ ++count +"个包子");
        flag=true;
        } catch (InterruptedException e) {
        e.printStackTrace();
        }finally {
            xiaofeizhe.signal();
            lock.unlock();

        }
    }
    public void xiaofei()
    {
        lock.lock();
        try {   while (flag!=true)
        { xiaofeizhe.await(); }
        System.out.println(Thread.currentThread().getName()+"消费第"+ count +"个包子");
        flag=false;
        } catch (InterruptedException e) {
        e.printStackTrace();
        }finally {
            proud.signal();
            lock.unlock();
        }
    }
}
public class MYBaoZi {
    public static void main(String[] args) {
        
        BaoZiPu baoZiPu=new BaoZiPu();

        new Thread(()->{
            while (true)
            {
                baoZiPu.shengcan();
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"小红——————————————————————————").start();
        new Thread(()->{
            while (true)
            {
                baoZiPu.shengcan();
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"小智*********").start();
        new Thread(()->{
            while (true)
            {
                baoZiPu.xiaofei();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"小光☆☆☆☆☆").start();
        new Thread(()->{
            while (true)
            {
                baoZiPu.xiaofei();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"小美").start();
    }
}
