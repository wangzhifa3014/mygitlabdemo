package com.atguigu.juc.Test1216.c_Lock;

public class Consumer implements Runnable{
    private BaoZiPu baoZiPu;
    public Consumer(BaoZiPu baoZiPu){
        this.baoZiPu = baoZiPu;
    }
    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(200L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            baoZiPu.getCount(baoZiPu);

        }
    }
}
