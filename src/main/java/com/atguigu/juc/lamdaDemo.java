package com.atguigu.juc;

/**
 * @author: lamdaDemo
 * @Desc:
 * @create: 2022-10-14 15:27
 **/
interface MyBook
        {
            int add(int a,int b);
            default void sum(int a,int b,int c)
            {
                System.out.println("静态");
            };
            static void sub(int a)
            {
                System.out.println(a);
            }
        }
interface MyBooks
{
    void add(int a,int b);
    default void sum(int a,int b,int c)
    {
        System.out.println("静态");
    };
    static void sub(int a)
    {
        System.out.println(a);
    }
}

public class lamdaDemo {
    public static void main(String[] args) {
        MyBook myBook=new MyBook() {
            @Override
            public int add(int a, int b) {
                return a+b;
            }
        };
        System.out.println(myBook.add(100,200));
        MyBook myBook1=
            (a,b) ->{
                System.out.println(a*b);
                return a+b+a;
            }
        ;
        System.out.println(myBook1.add(100,200));
        MyBooks myBooks=
                (a,b) -> System.out.println("a+b="+(a+b));
        myBooks.add(100,2);
    }
}
