package com.atguigu.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author: MYmutexTest
 * @Desc:
 * @create: 2022-10-19 16:48
 **/
class TuShu
{
    private int number=0;
    Mutex mutex=new Mutex();

    public void goushu()
    {
        mutex.lock();
        for (int i = 0; i < 100; i++) {
            number++;
            try {
                TimeUnit.MICROSECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(number);

        }
        mutex.unlock();
    }

}
public class MYmutexTest {
    public static void main(String[] args) {
        TuShu tuShu=new TuShu();
        CountDownLatch countDownLatch=new CountDownLatch(100);
        for (int i = 0; i < 100; i++) {
           new Thread(()->{tuShu.goushu();
           countDownLatch.countDown();}).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("购书成功");
    }
}
