package com.atguigu.juc;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author: MYutils
 * @Desc:
 * @create: 2022-10-18 19:02
 **/
public class MYutils {
    public static void main(String[] args) {
        //soujilongzhu();
        //youxitongguan();
        //youxi
        Semaphore semaphore = new Semaphore(4);
        for (int i = 0; i < 16; i++) {
            new Thread(()->{
                try {
                    Random random = new Random();
                    int k =random.nextInt(5)+3;
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"进入停车场"+"\t停车时间"+k);
                    TimeUnit.SECONDS.sleep(k*2);
                    System.out.println(Thread.currentThread().getName()+"离开停车场");
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i+1)).start();
        }

    }

   public static void youxi() {
       CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {

           System.out.println(Thread.currentThread().getName() + " 过关了");
       });

       for (int i = 0; i < 3; i++) {
           new Thread(() -> {
               try {
                   System.out.println(Thread.currentThread().getName() + " 开始第一关");
                   TimeUnit.SECONDS.sleep(new Random().nextInt(4));
                   System.out.println(Thread.currentThread().getName() + " 开始打boss");
                   cyclicBarrier.await();

                   System.out.println(Thread.currentThread().getName() + " 开始第二关");
                   TimeUnit.SECONDS.sleep(new Random().nextInt(4));
                   System.out.println(Thread.currentThread().getName() + " 开始打boss");
                   cyclicBarrier.await();

                   System.out.println(Thread.currentThread().getName() + " 开始第三关");
                   TimeUnit.SECONDS.sleep(new Random().nextInt(4));
                   System.out.println(Thread.currentThread().getName() + " 开始打boss");
                   cyclicBarrier.await();

               } catch (Exception e) {
                   e.printStackTrace();
               }
           }, String.valueOf(i)).start();
       }
   }
    public static void youxitongguan() {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(4, ()->{
                System.out.println(Thread.currentThread().getName()+"通过了一关");
        });
        for (int i = 0; i <4 ; i++) {
            new Thread(()->{
                Random r=new Random();

                for (int k = 1; k <= 3; k++) {
                    int j = r.nextInt(4)+3;
                    System.out.println("线程"+Thread.currentThread().getName()+"第"+k+"关战斗开始，废时"+j);
                    try {
                        TimeUnit.SECONDS.sleep(j);
                        System.out.println("线程"+Thread.currentThread().getName()+"成功打到第"+k+"关BOSS");
                        cyclicBarrier.await();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }

            },String.valueOf(i+1)).start();
        }
    }

    public static void soujilongzhu() {
        CountDownLatch countDownLatch = new CountDownLatch(7);
        for (int i = 0; i < 7; i++) {
            new Thread(()->{
                Random r=new Random();
                int j = r.nextInt(14)+3;
                System.out.println("收集龙珠费时"+j);
                try {
                    Thread.sleep(j*500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("成功收集到"+Thread.currentThread().getName()+"龙珠");
                countDownLatch.countDown();
            },String.valueOf(i+1)).start();
        }
        // 调用计算器的await方法，等待6位同学都出来
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("出来吧神龙");
    }
}
