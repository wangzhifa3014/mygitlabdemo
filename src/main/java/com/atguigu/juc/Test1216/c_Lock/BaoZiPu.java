package com.atguigu.juc.Test1216.c_Lock;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BaoZiPu {
    //定义一个count证明生产包子,消费包子
    private int count;
    //定义一个flag,证明有没有包子
    private boolean flag;

    public BaoZiPu() {
    }

    public BaoZiPu(int count, boolean flag) {
        this.count = count;
        this.flag = flag;
    }
     Lock lock=new ReentrantLock();
    Condition productCondition = lock.newCondition();
    //为消费者创建Condition对象
    Condition consumerCondition = lock.newCondition();
    //专门给消费线程服务
    public  void getCount(BaoZiPu baoZiPu) {
        //如果flag等于false.证明没有包子,消费线程等待
        lock.lock();
        while (baoZiPu.flag == false) {
            try {
                productCondition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //如果flag为true,证明有包子,消费包子,然后唤醒生活线程
        System.out.println(Thread.currentThread().getName()+"消费了第" + count + "个包子...........");
        //改变flag状态为false,证明没有包子了
        baoZiPu.flag = false;
        //唤醒生产线程
        consumerCondition.signal();
        lock.unlock();
    }

    //专门给生产线程服务
    public  void setCount(BaoZiPu baoZiPu) {
        //如果flag等于true.证明有包子,生产线程等待
        lock.lock();
        while (baoZiPu.flag == true) {
            try {
                consumerCondition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //如果flag为false,证明没有包子,生产包子,然后唤醒消费线程
        count++;
        System.out.println(Thread.currentThread().getName()+"生产了第" + count + "个包子....");
        //改变flag状态为true,证明有包子了
        baoZiPu.flag = true;
        //唤醒消费线程
        productCondition.signal();
        lock.unlock();
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
