package com.atguigu.juc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author: MYxiancheng
 * @Desc:
 * @create: 2022-10-18 16:18
 **/
class MYCallable implements Callable<Integer>
{

    @Override
    public Integer call() throws Exception {
        if(Thread.currentThread().getName().equals("小王"))
        {  System.out.println(Thread.currentThread().getName()+"1");
            Thread.sleep(4000);
        return 100;}
        else
        {
        System.out.println(Thread.currentThread().getName()+"call"+200);
            Thread.sleep(7000);
        return 200;
         }
}
}
public class MYxiancheng {

    public static void main(String[] args) {

        //clabos();
        List<String> list=new ArrayList<>();
        list.add("秦");
        list.add("楚");
        list.add("燕");
        list.add("赵");
        list.add("魏");
       int i;
        //for (String s : list) {
        for (i = 0; i <list.size(); i++) {

//            new Thread(()->{
//                System.out.println(i);
//                System.out.println(Thread.currentThread().getName());
//            },list.get(i)).start();
        }

        //}

    }

    public static void clabos() {
        FutureTask futureTask =new FutureTask(new MYCallable());
        new Thread(futureTask,"小王").start();
        new Thread(futureTask,"小化").start();

        Thread thread = new Thread(new FutureTask(() -> {
            System.out.println(Thread.currentThread().getName() + "100");
            Thread.sleep(2000);
            return 110;
        }), "wwwww");
        thread.start();
        System.out.println(futureTask.isDone()+"2");
        System.out.println(thread.isAlive()+"ba");
        //      while (!futureTask.isDone())
//        {
//            System.out.println("未执行结束");
//        }

        Object o=null;
        try {
            o = futureTask.get();
            System.out.println(futureTask.isDone()+"1");
            System.out.println(futureTask.get()+"1121");
            System.out.println(thread.isAlive()+"a");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println(o);
    }

}
