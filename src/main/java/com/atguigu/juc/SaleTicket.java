package com.atguigu.juc;

/**
 * @author: SaleTicket
 * @Desc:
 * @create: 2022-10-14 16:13
 **/
 class SaleBook
{
    volatile static int number=100;

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        SaleBook.number = number;
    }

    public synchronized  void sale()
    {


                if (number <= 0) {
                    System.out.println(Thread.currentThread().getName() + "抢票，但是票已经卖完");
                    return;
                }
        try {
                System.out.println(Thread.currentThread().getName() + "当前票数" + number);
                Thread.sleep(200);
                System.out.println(Thread.currentThread().getName() + "抢票成功，当前票数" + --number);
            } catch (Exception e) {
                e.printStackTrace();
            }


    }

    public   void sale1()
    {
        synchronized(SaleBook.class) {

                if (number <= 0) {
                    System.out.println(Thread.currentThread().getName() + "抢票，但是票已经卖完");
                    return;
                }

                System.out.println(Thread.currentThread().getName() + "当前票数" + number);
            try {
                Thread.sleep(200);
                System.out.println(Thread.currentThread().getName() + "抢票成功，当前票数" + --number);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
public class SaleTicket {
    public static void main(String[] args) {
        SaleBook saleBook=new SaleBook();
       new Thread(()->{
           while (1==1) {
               saleBook.sale();
               if(SaleBook.number<=0)
               {
                   System.out.println(Thread.currentThread().getName()+SaleBook.number);
                   break;
               }
               try {
                   Thread.sleep(100);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }

           }
       },"aaa").start();
        new Thread(()->{
            saleBook.sale();
            while (true) {
                saleBook.sale();
                if(SaleBook.getNumber()<=0)
                {
                    System.out.println(Thread.currentThread().getName()+SaleBook.number);
                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"bbb").start();
        new Thread(()->{
            saleBook.sale();
            while (true) {
                saleBook.sale();
                if(SaleBook.number<=0)
                {
                    System.out.println(Thread.currentThread().getName()+SaleBook.number);
                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"ccc").start();


        try {
//            while (true) {
//                if(SaleBook.number==0)
//                {
//                    Thread.sleep(2000);
//                    break;
//                }
//            }
            Thread.sleep(4000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        SaleBook.setNumber(SaleBook.getNumber()+200);
        new Thread(()->{

            while (true) {
                saleBook.sale1();
                if(SaleBook.number<=0)
                {
                    System.out.println(Thread.currentThread().getName()+SaleBook.number);
                    break;
                }
            }
        },"A").start();
        new Thread(()->{

            while (true) {
                saleBook.sale1();
                if(SaleBook.number<=0)
                {
                    System.out.println(Thread.currentThread().getName()+SaleBook.number);
                    break;
                }
            }
        },"B").start();
        new Thread(()->{

            while (true) {
                saleBook.sale1();
                if(SaleBook.number<=0)
                {
                    System.out.println(Thread.currentThread().getName()+SaleBook.number);
                    break;
                }
            }
        },"C").start();
    }

}
