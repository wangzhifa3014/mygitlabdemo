package com.atguigu.juc.Test1216.c_Lock;

public class Test {
    public static void main(String[] args) {
        //创建包子铺对象
        BaoZiPu baoZiPu = new BaoZiPu();

        Product product = new Product(baoZiPu);

        Consumer consumer = new Consumer(baoZiPu);

        //两个生产线程
        new Thread(product,"甲").start();
        new Thread(product,"乙").start();
        //两个消费线程
        new Thread(consumer,"A").start();
        new Thread(consumer,"B").start();
    }
}