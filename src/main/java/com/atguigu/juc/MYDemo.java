package com.atguigu.juc;

import java.lang.ref.WeakReference;
import java.util.UUID;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: MYDemo
 * @Desc:
 * @create: 2022-10-14 16:40
 **/
class Dayin
{
    private Lock lock=new ReentrantLock(true);
    Condition condition1 = lock.newCondition();
    Condition condition = lock.newCondition();
    boolean flag=true;
    int i=1;
    char a=65;
    public void shuzi()
    {

        lock.lock();
        try {
            while (flag!=true)
        { condition.await(); }
        for (int j = 0; j <2 ; j++) {
            System.out.print(Thread.currentThread().getName()+"i="+i++);
        }
            flag=false;
            //Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {condition1.signal();
            lock.unlock();

        }


    }
    public void zimu()
    {
        lock.lock();
        try {
            while(flag!=false)
            { condition1.await(); }
            System.out.print(Thread.currentThread().getName()+"a="+a++);
            System.out.println();
            flag=true;
            //Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }finally { condition.signal();
            lock.unlock();

        }


    }
}
public class MYDemo {


        public static WeakReference<String> weakReference1;

        public static void main(String[] args) {

            //ww();
            //wda();

            Dayin dayin=new Dayin();
            new Thread(() -> {
                for (int i = 0; i < 26; i++) {
                    dayin.shuzi();
                }
            }, "小红").start();
            new Thread(() -> {
                for (int i = 0; i < 26; i++) {
                    dayin.zimu();
                }
            }, "小光————————————————————").start();
        }

    public static void wda() {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + "i=\t" + i);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "小红");
        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + "i=\t" + i);
            }
        }, "小光");
        thread.start();
        try {

            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread2.start();
    }

    public static void ww() {
        test1();
        //test1外部，hello对象作用域结束，没有强引用指向"value"了。只有一个弱引用指向"value"
        System.out.println("未进行gc时，只有弱引用指向value内存区域：" + weakReference1.get());

        //此时gc时会回收弱引用
        System.gc();

        //此时输出都为nuill
        System.out.println("进行gc时，只有弱引用指向value内存区域：" + weakReference1.get());
    }

    public static void test1() {
            //hello对象强引用"value"
            String hello = new String("value");

            //weakReference1对象弱引用指向"value"
            weakReference1 = new WeakReference<>(hello);

            //在test1内部调用gc，此时gc不会回收弱引用，因为hello对象强引用"value"
            System.gc();
            System.out.println("进行gc时，强引用与弱引用同时指向value内存区域：" + weakReference1.get());

        }



}
